package com.mmall.controller.backend;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.pojo.User;
import com.mmall.service.IProductService;
import com.mmall.service.IUserService;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("/manage/product")

public class ProductManageController {
	@Autowired
	private IUserService iUserService;
	@Autowired
	private IProductService iProductService;

	
	
	/**
	 * 新增和更新商品
	 * 
	 * @param session会话
	 * @param product商品
	 * @param file上传的图片
	 * @param request
	 * @return
	 */
	@RequestMapping("save.do")
	@ResponseBody
	public ServerResponse productSave(HttpSession session, Product product,
			MultipartFile file, HttpServletRequest request) {
		User user = (User) session.getAttribute(Const.CURRENT_USER);// 获取当前用户
		if (user == null) {// 判断用户是否登录
			return ServerResponse.createByErrorCodeMessage(
					ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {// 判断用户是否是管理员登录
			if (product.getId() == null) {// 如果商品ID为空，进行商品上传
				Map resultMap = new HashedMap();// 实例化一个MAP集合来装上传的图片和商品信息
				boolean flag = uploadImg(file, request, resultMap);// 调用上传图片的方法
				if (flag) {
					// 给数据库里面的图片字段赋值，值就是图片的路径
					product.setMainImage(resultMap.get("path").toString());
					// 执行添加商品方法
					return iProductService.saveOrUpdateProduct(product);
				} else {// 返回错误信息
					return ServerResponse.createByErrorMessage(resultMap.get(
							"msg").toString());
				}
			} else {// 商品ID不等于空时，
				if (file != null) {// 图片不等于空时，重复上述上传图片的操作
					Map resultMap = new HashedMap();
					boolean flag = uploadImg(file, request, resultMap);
					if (flag) {
						product.setMainImage(resultMap.get("path").toString());
						// 执行更新商品信息操作
						return iProductService.saveOrUpdateProduct(product);
					} else {
						return ServerResponse.createByErrorMessage(resultMap
								.get("msg").toString());
					}
				} else {// 如果商品ID不等于空，执行更新的方法
					return iProductService.saveOrUpdateProduct(product);
				}
			}
		} else {// 不是管理员登录
			return ServerResponse.createByErrorMessage("无权限操作");
		}
	}

	// 编写上传图片的方法uploadImg
	public boolean uploadImg(MultipartFile file, HttpServletRequest request,
			Map resultMap) {
		if (file == null) {
			resultMap.put("msg", "请选择图片");
			return false;
		}
		String fileName = file.getOriginalFilename();// 文件原名称
		String pix = fileName.substring(fileName.lastIndexOf("."))
				.toLowerCase();
		System.out.println(pix);
		if (pix.equals(".png") || pix.equals(".jpg") || pix.equals(".gif")) {
			String realPath = request.getSession().getServletContext()
					.getRealPath("/");
			// 自定义的文件名称
			String trueFileName = String.valueOf(System.currentTimeMillis())
					+ pix;
			// 设置存放图片文件的路径
			String path = realPath + "img\\" + trueFileName;
			System.out.println("存放图片文件的路径:" + path);
			// 转存文件到指定的路径
			try {
				file.transferTo(new File(path));
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("文件成功上传到指定目录下");
			resultMap.put("path", trueFileName);
			return true;
		} else {
			resultMap.put("msg", "图片格式错误");
			return false;
		}
	}
	/**
	 * 商品上下架
	 * @param session
	 * @param productId商品ID
	 * @param status销售状态
	 * @return
	 */
	@RequestMapping("set_sale_status.do")
	@ResponseBody
	public ServerResponse setSaleStatus(HttpSession session, Integer productId,
			Integer status) {
		//获取当前用户
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByErrorCodeMessage(
					ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");

		}
		if (iUserService.checkAdminRole(user).isSuccess()) {//验证是否是管理员
			//调用上下架方法
			return iProductService.setSaleStatus(productId, status);
		} else {
			return ServerResponse.createByErrorMessage("无权限操作");
		}
	}
	/**
	 * 商品详细
	 * @param session会话
	 * @param productId商品ID
	 * @return
	 */
	@RequestMapping("detail.do")
	@ResponseBody
	public ServerResponse getDetail(HttpSession session, Integer productId) {
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByErrorCodeMessage(
					ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			//调用查询详细的方法
			return iProductService.manageProductDetail(productId);
		} else {
			return ServerResponse.createByErrorMessage("无权限操作");
		}
	}
	/**
	 * 商品列表
	 * @param session
	 * @param pageNum当前页
	 * @param pageSize每页条数
	 * @return
	 */
	@RequestMapping("list.do")
	@ResponseBody
	public ServerResponse getList(HttpSession session,
			@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByErrorCodeMessage(
					ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");

		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 填充业务
			return iProductService.getProductList(pageNum, pageSize);
		} else {
			return ServerResponse.createByErrorMessage("无权限操作");
		  }
	    }
		/**
		 * 搜索功能
		 * @param session
		 * @param productName根据商品名模糊查询商品
		 * @param productId根据商品ID查询商品
		 * @param pageNum当前页，默认值为第一页
		 * @param pageSize当前页条数，默认显示10条
		 * @return
		 */
		@RequestMapping("search.do")
		@ResponseBody
		public ServerResponse productSearch(HttpSession session,
				String productName, Integer productId,
				@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
				@RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
			User user = (User) session.getAttribute(Const.CURRENT_USER);
			if (user == null) {
				return ServerResponse.createByErrorCodeMessage(
						ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");

			}
			if (iUserService.checkAdminRole(user).isSuccess()) {
				// 填充业务
				return iProductService.searchProduct(productName, productId,
						pageNum, pageSize);
			} else {
				return ServerResponse.createByErrorMessage("无权限操作");
			}
		}
}
