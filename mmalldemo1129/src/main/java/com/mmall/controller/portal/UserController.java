package com.mmall.controller.portal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;

@Controller
@RequestMapping("/user/")
public class UserController {
	@Autowired
	private IUserService iUserService;
	/**
	 * 用户登录
	 * @param username
	 * @param password
	 * @param session
	 * @return
	 */
	 @RequestMapping(value = "login.do",method = RequestMethod.POST)
	    @ResponseBody
	    public ServerResponse<User> login(String username, String password, HttpSession session){
	        ServerResponse<User> response = iUserService.login(username,password);
	        if(response.isSuccess()){
	            session.setAttribute(Const.CURRENT_USER,response.getData());
	        }
	        return response;
	    }
      //用户退出	 
	   @RequestMapping(value="logout.do",method=RequestMethod.GET)
	   @ResponseBody
	    public ServerResponse<String> logout(HttpSession session){
		   session.removeAttribute(Const.CURRENT_USER);
		   return ServerResponse.createBySuccess("退出成功");
	   }
	   /**
		  * 用户注册
		  * @param user
		  * @return
		  */
		 @RequestMapping(value = "register.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> register(User user){
		        return iUserService.register(user);
		    }

		 	/**
		 	 * 用户校验方法
		 	 * @param str
		 	 * @param type
		 	 * @return
		 	 */
		    @RequestMapping(value = "check_valid.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> checkValid(String str,String type){
		        return iUserService.checkValid(str,type);
		    }
		    /**
		     * 获取用户登录信息
		     * @param session从session中获取
		     * @return
		     */
		    @RequestMapping(value = "get_user_info.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<User> getUserInfo(HttpSession session){
		        User user = (User) session.getAttribute(Const.CURRENT_USER);
		        if(user != null){
		            return ServerResponse.createBySuccess(user);
		        }
		        return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户的信息");
		    }
		    /**
		     * 忘记密码之问题
		     * @param username
		     * @return
		     */
		    @RequestMapping(value = "forget_get_question.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> forgetGetQuestion(String username){
		        return iUserService.selectQuestion(username);
		    }
		    /**
		     * 忘记密码之答案
		     * @param username
		     * @param question
		     * @param answer
		     * @return
		     *201314的uuid636ecf63-d6b8-4191-83b4-4fc91894bd3b
		     */
		    @RequestMapping(value = "forget_check_answer.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> forgetCheckAnswer(String username,String question,String answer){
		        return iUserService.checkAnswer(username,question,answer);
		    }
		    /**
		     * 未登录状态下忘记密码之重置密码
		     * @param username用户名
		     * @param passwordNew新密码
		     * @param forgetToken本地token信息
		     * @return
		     */
		    @RequestMapping(value = "forget_reset_password.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> forgetRestPassword(String username,String passwordNew,String forgetToken){
		        return iUserService.forgetResetPassword(username,passwordNew,forgetToken);
		    }
		    /**
		     * 登录状态下重置密码
		     * @param session通过session获取当前用户
		     * @param passwordOld旧密码
		     * @param passwordNew新密码
		     * @return
		     */
		    @RequestMapping(value = "reset_password.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<String> resetPassword(HttpSession session,String passwordOld,String passwordNew){
		        User user = (User)session.getAttribute(Const.CURRENT_USER);
		        if(user == null){
		            return ServerResponse.createByErrorMessage("用户未登录");
		        }
		        return iUserService.resetPassword(passwordOld,passwordNew,user);
		    }
		    /**
		     * 更新用户信息
		     * @param session
		     * @param user
		     * @return
		     */
		    @RequestMapping(value = "update_information.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<User> update_information(HttpSession session,User user){
		        User currentUser = (User)session.getAttribute(Const.CURRENT_USER);
		        if(currentUser == null){
		            return ServerResponse.createByErrorMessage("用户未登录");
		        }
		        user.setId(currentUser.getId());
		        user.setUsername(currentUser.getUsername());
		        ServerResponse<User> response = iUserService.updateInformation(user);
		        if(response.isSuccess()){
		            response.getData().setUsername(currentUser.getUsername());
		            session.setAttribute(Const.CURRENT_USER,response.getData());
		        }
		        return response;
		    }
		    /**
		     * 根据ID查询用户详细信息
		     * @param session
		     * @return
		     */
		    @RequestMapping(value = "get_information.do",method = RequestMethod.POST)
		    @ResponseBody
		    public ServerResponse<User> get_information(HttpSession session){
		        User currentUser = (User)session.getAttribute(Const.CURRENT_USER);
		        if(currentUser == null){
		            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"未登录,需要强制登录status=10");
		        }
		        return iUserService.getInformation(currentUser.getId());
		    }

}