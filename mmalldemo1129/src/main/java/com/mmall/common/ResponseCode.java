package com.mmall.common;
//定义一个枚举来响应编码
public enum ResponseCode {
	SUCCESS(0,"SUCCESS"),//成功
	ERROR(1,"ERROR"),//失败
	NEED_LOGIN(10,"NEED_LOGIN"),//需要登录
	ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT");//参数错误
	
	//声明属性
	private final int code;//成功代码
	private final String desc;//错误描述
	
	ResponseCode(int code,String desc){
		this.code=code;
		this.desc=desc;
		
	}
	//开放code和desc
	public int getCode(){
		return code;
	}
	public String getDesc(){
		return desc;
	}
}


