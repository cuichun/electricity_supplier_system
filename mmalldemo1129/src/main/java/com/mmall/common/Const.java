package com.mmall.common;

import java.util.Set;

import com.google.common.collect.Sets;

public class Const {
	//定义一个最近/当前的用户常量
		public static final String CURRENT_USER="currentUser";
		//定义验证邮箱、用户名
		public static final String CURRENT_Eamil="eamil";  
		public static final String USERNAME="username";
		//权限
		public interface Role{
			int ROLE_COUTOMER=0;//用户
			int ROLE_ADMIN=1;//管理员	
		}
		//商品销售状态
	    public enum ProductStatusEnum{
	        ON_SALE(1,"在线");
	        private String value;
	        private int code;
	        ProductStatusEnum(int code,String value){
	            this.code = code;
	            this.value = value;
	        }

	        public String getValue() {
	            return value;
	        }

	        public int getCode() {
	            return code;
	        }
	    }
	  //商品根据价格动态排序
	    public interface ProductListOrderBy{
	        Set<String> PRICE_ASC_DESC = Sets.newHashSet("price_desc","price_asc");
	    }
	  //购物车
	    public interface Cart{
	        int CHECKED = 1;//即购物车选中状态
	        int UN_CHECKED = 0;//购物车中未选中状态
	        //添加商品数量失败
	        String LIMIT_NUM_FAIL = "LIMIT_NUM_FAIL";
	        //添加商品数量成功
	        String LIMIT_NUM_SUCCESS = "LIMIT_NUM_SUCCESS";
			Integer getId();
			double getQuantity();
			Integer getProductId();
			
	    }
	  //订单状态
	    public enum OrderStatusEnum{
	        CANCELED(0,"已取消"),
	        NO_PAY(10,"未支付"),
	        PAID(20,"已付款"),
	        SHIPPED(40,"已发货"),
	        ORDER_SUCCESS(50,"订单完成"),
	        ORDER_CLOSE(60,"订单关闭");


	        OrderStatusEnum(int code,String value){
	            this.code = code;
	            this.value = value;
	        }
	        private String value;
	        private int code;

	        public String getValue() {
	            return value;
	        }

	        public int getCode() {
	            return code;
	        }

	        public static OrderStatusEnum codeOf(int code){
	            for(OrderStatusEnum orderStatusEnum : values()){
	                if(orderStatusEnum.getCode() == code){
	                    return orderStatusEnum;
	                }
	            }
	            throw new RuntimeException("么有找到对应的枚举");
	        }
	    }
	   



	    //支付方式
	    public enum PaymentTypeEnum{
	        ONLINE_PAY(1,"在线支付");

	        PaymentTypeEnum(int code,String value){
	            this.code = code;
	            this.value = value;
	        }
	        private String value;
	        private int code;

	        public String getValue() {
	            return value;
	        }

	        public int getCode() {
	            return code;
	        }


	        public static PaymentTypeEnum codeOf(int code){
	            for(PaymentTypeEnum paymentTypeEnum : values()){
	                if(paymentTypeEnum.getCode() == code){
	                    return paymentTypeEnum;
	                }
	            }
	            throw new RuntimeException("么有找到对应的枚举");
	        }

	    }
}
