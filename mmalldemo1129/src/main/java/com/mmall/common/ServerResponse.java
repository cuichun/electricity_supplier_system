package com.mmall.common;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;



/**
 * 
 * @author 樱
 *ServerResponse通用的数据端的响应对象
 *<T>,用一个泛型来封装返回的数据类型
 *implements Serializable实现序列化接口
 *@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)表示没有data时，为空不显示在json里面
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {

	private int status;//状态
	private String msg;//提示信息
	private T data;//泛型数据对象data
	
	//编写构造方法
	private ServerResponse(int status) {
		this.status=status;
	}
	private ServerResponse(int status,T data) {
		this.status=status;
		this.data=data;
	}
	private ServerResponse(int status,String msg,T data) {
		this.status=status;
		this.msg=msg;
		this.data=data;
	}
	private ServerResponse(int status,String msg) {
		this.status=status;
		this.msg=msg;
	}
	
	//对外开放的成功方法
	//ResponseCode.SUCCESS.getCode()对象的响应状态编码
	//这个注解@JsonIgnore意思是不要让isSuccess()现实在json数据里面
	@JsonIgnore
	public boolean isSuccess(){
		return this.status==ResponseCode.SUCCESS.getCode();
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getMsg(){
		return msg;
	}
	public T getData(){
		return data;
	}
	
	//创建一个静态的成功方法，返回一个成功后的对象
	//status
	 public static <T> ServerResponse<T> createBySuccess(){
	        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
	    }
	 	//返回 msg
	    public static <T> ServerResponse<T> createBySuccessMessage(String msg){
	        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg);
	    }
	    //返回data
	    public static <T> ServerResponse<T> createBySuccess(T data){
	        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data);
	    }
	    //返回msg, data
	    public static <T> ServerResponse<T> createBySuccess(String msg,T data){
	        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg,data);
	    }
	    
	    
	    //错误时
	    public static <T> ServerResponse<T> createByError(){
	        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
	    }


	    public static <T> ServerResponse<T> createByErrorMessage(String errorMessage){
	        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),errorMessage);
	    }

	    public static <T> ServerResponse<T> createByErrorCodeMessage(int errorCode,String errorMessage){
	        return new ServerResponse<T>(errorCode,errorMessage);
	    }
}

