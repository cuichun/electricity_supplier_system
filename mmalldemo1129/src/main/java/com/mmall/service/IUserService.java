package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;

public interface IUserService {
	// 编写IUserService.java接口中的登录：

	ServerResponse<User> login(String username, String password);

	// 用户注册
	ServerResponse<String> register(User user);

	// 验证用户名和邮箱，type指的是用户名和邮箱
	ServerResponse<String> checkValid(String str, String type);

	// 根据用户名查询用户问题
	ServerResponse selectQuestion(String username);

	// 验证问题答案
	ServerResponse<String> checkAnswer(String username, String question,
			String answer);

	// 重置密码
	ServerResponse<String> forgetResetPassword(String username,
			String passwordNew, String forgetToken);

	// 登录状态下重置密码
	ServerResponse<String> resetPassword(String passwordOld,
			String passwordNew, User user);
	//更新用户信息
	 ServerResponse<User> updateInformation(User user);
	 //根据id的查询用户信息
	 
	 ServerResponse<User> getInformation(Integer userid);
	 
	 //验证用户是管理员
	 ServerResponse<User> checkAdminRole( User user);
	 
}
