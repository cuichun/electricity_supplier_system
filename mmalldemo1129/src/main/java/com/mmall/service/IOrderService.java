package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Order;
import com.mmall.vo.OrderVo;

public interface IOrderService {
	//创建订单
    ServerResponse createOrder(Integer userId,Integer shippingId);
    //取消订单
    ServerResponse<String> cancel(Integer userId,Long orderNo);
    //获取订单的商品信息
    ServerResponse getOrderCartProduct(Integer userId);
    //查看订单详细
    ServerResponse<OrderVo> getOrderDetail(Integer userId, Long orderNo);
    //查看订单列表，带分页
    ServerResponse<PageInfo> getOrderList(Integer userId, int pageNum, int pageSize);
  //后台管理
    //订单列表，带分页
    ServerResponse<PageInfo> manageList(int pageNum,int pageSize);
    //后台订单详细
    ServerResponse<OrderVo> manageDetail(Long orderNo);
    //后台订单搜索
    ServerResponse<PageInfo> manageSearch(Long orderNo,int pageNum,int pageSize);
    //后台订单发货
    ServerResponse<String> manageSendGoods(Long orderNo);


}
