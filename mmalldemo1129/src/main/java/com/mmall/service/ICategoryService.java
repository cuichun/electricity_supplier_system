package com.mmall.service;

import java.util.List;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.Category;
//品类接口
public interface ICategoryService {
	//添加品类
    ServerResponse addCategory(String categoryName, Integer parentId);
     //修改类名
    ServerResponse updateCategoryName(Integer categoryId,String categoryName);
    //查询品类子节点(平级)，返回的是一个集合
    ServerResponse<List<Category>> getChildrenParallelCategory(Integer categoryId);
    //获取当前分类id及递归子节点categoryId
    ServerResponse<List<Integer>> selectCategoryAndChildrenById(Integer categoryId);
}

