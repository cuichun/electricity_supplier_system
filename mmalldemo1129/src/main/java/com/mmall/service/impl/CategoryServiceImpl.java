package com.mmall.service.impl;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.pojo.Category;
import com.mmall.service.ICategoryService;

/**
 * 品类功能实现类
 * 
 * @author 樱
 * 
 */
@Service("iCategoryService")
public class CategoryServiceImpl implements ICategoryService {
	 private Logger logger=LoggerFactory.getLogger(CategoryServiceImpl.class);
	@Autowired
	private CategoryMapper categoryMapper;

	/*
	 * addCategory添加分类
	 */
	public ServerResponse addCategory(String categoryName, Integer parentId) {
		// 判断输入的父类别ID是否为空或者品类名是否为空或空字符
		if (parentId == null || StringUtils.isBlank(categoryName)) {
			return ServerResponse.createByErrorMessage("添加品类参数错误");
		}

		Category category = new Category();// 实例化一个品类
		category.setName(categoryName);
		category.setParentId(parentId);
		category.setStatus(true);// 这个分类是可用的
		// 执行insert方法添加品类
		int rowCount = categoryMapper.insert(category);
		if (rowCount > 0) {// 当添加的条数大于零，代表添加成功
			return ServerResponse.createBySuccess("添加品类成功");
		}
		return ServerResponse.createByErrorMessage("添加品类失败");
	}

	// 修改品类名称
	@Override
	public ServerResponse updateCategoryName(Integer categoryId,
			String categoryName) {

		// 判断父类别ID是否为空或者品类名是否为空或空字符
		if (categoryId == null || StringUtils.isBlank(categoryName)) {
			return ServerResponse.createByErrorMessage("更新品类参数错误");
		}
		Category category = new Category();
		category.setId(categoryId);
		category.setName(categoryName);
		// 执行选择性更新品类方法
		int rowCount = categoryMapper.updateByPrimaryKeySelective(category);
		if (rowCount > 0) {
			return ServerResponse.createBySuccess("更新品类名字成功");
		      }
		return ServerResponse.createByErrorMessage("更新品类名字失败");
	      }
	//获取品类子节点(平级)
		@Override
		public ServerResponse<List<Category>> getChildrenParallelCategory(
				Integer categoryId) {
			//调用查询节点方法进行查询
			 List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
		        //判断查询出来的categoryList集合是否为空
			 if(CollectionUtils.isEmpty(categoryList)){
		            logger.info("未找到当前分类的子分类");
		        }
		        return ServerResponse.createBySuccess(categoryList);
		}

		/**
	     * 递归查询本节点的id及孩子节点的id
	     * @param categoryId品类ID
	     * @return
	     */
	    public ServerResponse<List<Integer>> selectCategoryAndChildrenById(Integer categoryId){
	        Set<Category> categorySet = Sets.newHashSet();//HashSet只能存储不重复的对象
	        findChildCategory(categorySet,categoryId);//调用递归算法
	        //ArrayList是List接口的具体实现类,可以存储任何元素,包括null
	        List<Integer> categoryIdList = Lists.newArrayList();
	        if(categoryId != null){//遍历查询出来的结果
	            for(Category categoryItem : categorySet){
	            //把获取到的孩子节点的id添加到categoryIdList集合中
	                categoryIdList.add(categoryItem.getId());
	            }
	        }
	        return ServerResponse.createBySuccess(categoryIdList);
	    }


	    //递归算法,算出子节点
	    private Set<Category> findChildCategory(Set<Category> categorySet ,Integer categoryId){
	        //根据categoryId品类ID查询品类
	    	Category category = categoryMapper.selectByPrimaryKey(categoryId);
	        if(category != null){//当品类不为空时添加到set集合中
	            categorySet.add(category);
	        }
	        //查找子节点,递归算法一定要有一个退出的条件
	        //获取品类子节点(平级)
	        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
	        for(Category categoryItem : categoryList){//遍历品类子节点
	            findChildCategory(categorySet,categoryItem.getId());
	        }
	        return categorySet;
	    }
	  
    }
