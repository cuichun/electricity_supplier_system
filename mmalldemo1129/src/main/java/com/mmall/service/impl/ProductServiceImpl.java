package com.mmall.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Category;
import com.mmall.pojo.Product;
import com.mmall.service.ICategoryService;
import com.mmall.service.IProductService;
import com.mmall.util.DateTimeUtil;
import com.mmall.vo.ProductDetailVo;
import com.mmall.vo.ProductListVo;

@Service("IProductService")
public class ProductServiceImpl implements IProductService{
	@Autowired
	private ProductMapper productMapper; 
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private ICategoryService iCategoryService;
	    /**
	     * 商品新增和更新
	     */
	    public ServerResponse saveOrUpdateProduct(Product product){
	        if(product != null)//判断商品不等于空
	        {

	            if(product.getId() != null){//判断商品ID不等于空
	            	//执行更新大方法
	                int rowCount = productMapper.updateByPrimaryKey(product);
	                if(rowCount > 0){
	                    return ServerResponse.createBySuccess("更新产品成功");
	                }
	                return ServerResponse.createBySuccess("更新产品失败");
	            }else{//否则商品为空，进行添加商品操作
	                int rowCount = productMapper.insert(product);
	                if(rowCount > 0){
	                    return ServerResponse.createBySuccess("新增产品成功");
	                }
	                return ServerResponse.createBySuccess("新增产品失败");
	            }
	        }
	        return ServerResponse.createByErrorMessage("新增或更新产品参数不正确");
	    }
	  //商品上下架方法
	    public ServerResponse<String> setSaleStatus(Integer productId,Integer status){
	        if(productId == null || status == null){//判断商品ID等于空或者销售状态为空时，返回参数错误
	            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
	        }
	       //否则对商品的ID和销售状态重新赋值
	        Product product = new Product();
	        product.setId(productId);
	        product.setStatus(status);
	        //执行选择性更新商品方法
	        int rowCount = productMapper.updateByPrimaryKeySelective(product);
	        if(rowCount > 0){
	            return ServerResponse.createBySuccess("修改产品销售状态成功");
	        }
	        return ServerResponse.createByErrorMessage("修改产品销售状态失败");
	    }
	    /**
	     * manageProductDetail后台管理商品详细
	     */
	    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId){
	        if(productId == null){//判断输入查询的productId商品ID是否为空
	            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
	        }
	        //商品ID不为空，执行查询
	        Product product = productMapper.selectByPrimaryKey(productId);
	        if(product == null){//判断根据商品ID查询出的商品是否为空
	            return ServerResponse.createByErrorMessage("产品已下架或者删除");
	        }
	        //不为空就把查询出来的商品信息赋给productDetailVo对象
	        ProductDetailVo productDetailVo = assembleProductDetailVo(product);
	       //返回查询出来的商品详细
	        return ServerResponse.createBySuccess(productDetailVo);
	    }
	    
	    //将查询到的值赋给ProductDetailVo商品详细对象
	    private ProductDetailVo assembleProductDetailVo(Product product){
	        ProductDetailVo productDetailVo = new ProductDetailVo();
	        productDetailVo.setId(product.getId());
	        productDetailVo.setSubtitle(product.getSubtitle());
	        productDetailVo.setPrice(product.getPrice());
	        productDetailVo.setMainImage(product.getMainImage());
	        productDetailVo.setSubImages(product.getSubImages());
	        productDetailVo.setCategoryId(product.getCategoryId());
	        productDetailVo.setDetail(product.getDetail());
	        productDetailVo.setName(product.getName());
	        productDetailVo.setStatus(product.getStatus());
	        productDetailVo.setStock(product.getStock());

	        //查询得到品类ID
	        Category category = Category.selectByPrimaryKey(product.getCategoryId());
	        if(category == null){
	            productDetailVo.setParentCategoryId(0);//默认根节点
	        }else{
	            productDetailVo.setParentCategoryId(category.getParentId());
	        }
	        //处理时间格式，需要用到util包下的时间工具类DateTimeUtil.java
	        productDetailVo.setCreateTime(DateTimeUtil.dateToStr(product.getCreateTime()));
	        productDetailVo.setUpdateTime(DateTimeUtil.dateToStr(product.getUpdateTime()));
	        return productDetailVo;//返回商品详细对象
	    }
	    /**
	     * 分页查询列表
	     * PageInfo封装了分页信息
	     */
		@Override
		public ServerResponse<PageInfo> getProductList(int pageNum,int pageSize){
	        PageHelper.startPage(pageNum,pageSize);//pageHelper-分页
	      //填充自己的sql查询逻辑
	        List<Product> productList = productMapper.selectList();

	        List<ProductListVo> productListVoList = Lists.newArrayList();
	        for(Product productItem : productList){//遍历查询到的商品
	            ProductListVo productListVo = assembleProductListVo(productItem);
	          //将得到商品添加到productListVo中
	            productListVoList.add(productListVo);
	        }
	        //实例化分页对象pageResult
	        PageInfo pageResult = new PageInfo(productList);
	        //分页赋值给productListVoList
	        pageResult.setList(productListVoList);
	        return ServerResponse.createBySuccess(pageResult);
	    }
		
		//将查询出的商品信息赋值给ProductListVo对象
	    private ProductListVo assembleProductListVo(Product product){
	        ProductListVo productListVo = new ProductListVo();
	        productListVo.setId(product.getId());
	        productListVo.setName(product.getName());
	        productListVo.setCategoryId(product.getCategoryId());
	        productListVo.setMainImage(product.getMainImage());
	        productListVo.setPrice(product.getPrice());
	        productListVo.setSubtitle(product.getSubtitle());
	        productListVo.setStatus(product.getStatus());
	        return productListVo;
	    }
	    /**
	     * 根据商品名称模糊搜索商品和根据ID搜索商品
	     */
	    public ServerResponse<PageInfo> searchProduct(String productName,Integer productId,int pageNum,int pageSize){
	        PageHelper.startPage(pageNum,pageSize);
	        if(StringUtils.isNotBlank(productName)){//判断商品名是否为空
	        	//构造商品名为%号开头，%号结尾，方便模糊查询
	            productName = new StringBuilder().append("%").append(productName).append("%").toString();
	        }
	        //通过商品名和商品ID执行查询
	        List<Product> productList = productMapper.selectByNameAndProductId(productName,productId);
	        List<ProductListVo> productListVoList = Lists.newArrayList();
	        for(Product productItem : productList){//遍历查询结果
	            ProductListVo productListVo = assembleProductListVo(productItem);
	            //将查询结果添加到productListVo对象中
	            productListVoList.add(productListVo);
	        }
	        //实例化分页对象pageResult
	        PageInfo pageResult = new PageInfo(productList);
	        //分页信息赋值给productListVoList
	        pageResult.setList(productListVoList);
	        return ServerResponse.createBySuccess(pageResult);
	    }
	    /**
	     * 实现前台商品模糊搜索，根据商品品类ID搜索，列表分页，根据商品价格动态排序的方法
	     */
		@Override
		public ServerResponse<PageInfo> getProductByKeywordCategory(String keyword,Integer categoryId,int pageNum,int pageSize,String orderBy){
	        if(StringUtils.isBlank(keyword) && categoryId == null){
	        	//判断输入的keyword关键字和品类ID是否为空，空则参数错误
	            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
	        }
	        //实例化品类ID列表
	        List<Integer> categoryIdList = new ArrayList<Integer>();

	        if(categoryId != null){//如果品类ID不为空，执行根据品类ID查询
	            Category category = categoryMapper.selectByPrimaryKey(categoryId);
	            if(category == null && StringUtils.isBlank(keyword)){
	                //没有该分类,并且还没有关键字,这个时候返回一个空的结果集,不报错
	                PageHelper.startPage(pageNum,pageSize);
	                List<ProductListVo> productListVoList = Lists.newArrayList();
	                PageInfo pageInfo = new PageInfo(productListVoList);
	                return ServerResponse.createBySuccess(pageInfo);
	            }
	            //执行获取当前分类id及递归子节点categoryId的SQL语句
	            categoryIdList = iCategoryService.selectCategoryAndChildrenById(category.getId()).getData();
	        }
	        if(StringUtils.isNotBlank(keyword)){//拼接输入的关键字，方便SQL模糊擦护心
	            keyword = new StringBuilder().append("%").append(keyword).append("%").toString();
	        }
	        //开始进行分页
	        PageHelper.startPage(pageNum,pageSize);
	        //排序处理
	        if(StringUtils.isNotBlank(orderBy)){
	            if(Const.ProductListOrderBy.PRICE_ASC_DESC.contains(orderBy)){
	            	//orderBy排序规则是price desc形式，所以要用split方法去掉"_"
	                String[] orderByArray = orderBy.split("_");
	                //分页排序
	                PageHelper.orderBy(orderByArray[0]+" "+orderByArray[1]);
	            }
	        }
	        //执行商品名称模糊查询和根据商品品类遍历和只查询上架的商品的SQL语句
	        List<Product> productList = productMapper.selectByNameAndCategoryIds(StringUtils.isBlank(keyword)?null:keyword,categoryIdList.size()==0?null:categoryIdList);

	        List<ProductListVo> productListVoList = Lists.newArrayList();
	        for(Product product : productList){//遍历查询出的商品对象
	            ProductListVo productListVo = assembleProductListVo(product);
	            //将遍历的商品对象添加到productListVoList列表
	            productListVoList.add(productListVo);
	        }

	        //实例化分页对象pageResult
	        PageInfo pageInfo = new PageInfo(productList);
	        //分页信息赋值给productListVoList
	        pageInfo.setList(productListVoList);
	        //返回分页信息
	        return ServerResponse.createBySuccess(pageInfo);
	    }
		@Override
		public ServerResponse<ProductDetailVo> getProductDetail(
				Integer productId) {
			// TODO Auto-generated method stub
			return null;
		}
	   
}
