package com.mmall.service.impl;




import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CartMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Cart;
import com.mmall.pojo.Product;
import com.mmall.service.ICartService;
import com.mmall.util.BigDecimalUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.CartProductVo;
import com.mmall.vo.CartVo;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
    @Service("iCartService")
    public class CartServiceImpl implements ICartService {

        @Autowired
        private CartMapper cartMapper;
        @Autowired
        private ProductMapper productMapper;

        /**
         * 添加商品到购物车方法实现
         */
        //1
        public ServerResponse<CartVo> add(Integer userId,Integer productId,Integer count){
            if(productId == null || count == null){
                return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
            }

            //查询商品是否在购物车中
            Cart cart = cartMapper.selectCartByUserIdProductId(userId,productId);
            if(cart == null){
                //这个产品不在这个购物车里,需要新增一个这个产品的记录
                Cart cartItem = new Cart();
                cartItem.setQuantity(count);
                cartItem.setChecked(Const.Cart.CHECKED);
                cartItem.setProductId(productId);
                cartItem.setUserId(userId);
                cartMapper.insert(cartItem);
            }else{
                //这个产品已经在购物车里了.
                //如果产品已存在,数量相加
                count = cart.getQuantity() + count;
                cart.setQuantity(count);
                cartMapper.updateByPrimaryKeySelective(cart);
            }
            CartVo cartVo=this.getCartVoLimit(userId);
            return ServerResponse.createBySuccess(cartVo);
            //return this.list(userId);
        }

        //限制购物车中的商品
        private CartVo getCartVoLimit(Integer userId){
            CartVo cartVo = new CartVo();
            //通过用户ID查询购物车
            List<Cart> cartList = cartMapper.selectCartByUserId(userId);
            List<CartProductVo> cartProductVoList = Lists.newArrayList();
            //设置购物车中商品总价格，初始值为0
            BigDecimal cartTotalPrice = new BigDecimal("0");
            
            if(CollectionUtils.isNotEmpty(cartList)){
                for(Cart cartItem : cartList){//如果购物车不为空，则遍历购物车中的商品
                    CartProductVo cartProductVo = new CartProductVo();
                    cartProductVo.setId(cartItem.getId());
                    cartProductVo.setUserId(userId);
                    cartProductVo.setProductId(cartItem.getProductId());
                    //根据商品ID查询商品
                    Product product = productMapper.selectByPrimaryKey(cartItem.getProductId());
                    if(product != null){//如果商品不为空，遍历商品信息
                        cartProductVo.setProductMainImage(product.getMainImage());
                        cartProductVo.setProductName(product.getName());
                        cartProductVo.setProductSubtitle(product.getSubtitle());
                        cartProductVo.setProductStatus(product.getStatus());
                        cartProductVo.setProductPrice(product.getPrice());
                        cartProductVo.setProductStock(product.getStock());
                        //判断库存
                        int buyLimitCount = 0;//购物车商品总数量
                        //库存大于等于商品所选数量时
                        if(product.getStock() >= cartItem.getQuantity()){
                            //库存充足的时候
                            buyLimitCount = cartItem.getQuantity();
                            //添加商品数量成功
                            cartProductVo.setLimitQuantity(Const.Cart.LIMIT_NUM_SUCCESS);
                        }else{//否则，库存不足时获取当前库存
                            buyLimitCount = product.getStock();
                            //添加商品数量失败
                            cartProductVo.setLimitQuantity(Const.Cart.LIMIT_NUM_FAIL);
                            //购物车中更新有效库存
                            Cart cartForQuantity = new Cart();
                            cartForQuantity.setId(cartItem.getId());
                            cartForQuantity.setQuantity(buyLimitCount);
                            cartMapper.updateByPrimaryKeySelective(cartForQuantity);
                        }
                       //将购物车单个商品总数量赋给数量字段
                        cartProductVo.setQuantity(buyLimitCount);
                        //计算总价，采用计算单价方法BigDecimalUtil.mul（）中的乘法，价格乘以数量
                        cartProductVo.setProductTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(),cartProductVo.getQuantity()));
                        //勾选购物车中的商品
                        cartProductVo.setProductChecked(cartItem.getChecked());
                    }

                    if(cartItem.getChecked() == Const.Cart.CHECKED){
                        //如果已经勾选,增加到整个的购物车总价中
                        cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(),cartProductVo.getProductTotalPrice().doubleValue());
                    }
                    cartProductVoList.add(cartProductVo);
                }
            }
            cartVo.setCartTotalPrice(cartTotalPrice);//填充购物车中商品总价格
            cartVo.setCartProductVoList(cartProductVoList);//填充购物车中商品列表对象
            cartVo.setAllChecked(this.getAllCheckedStatus(userId));//填充所勾选的商品        
            return cartVo;
        }
        //2
        //验证购物车中商品是否勾选
        private boolean getAllCheckedStatus(Integer userId){
            if(userId == null){
                return false;
            }
            //对查询出来的结果进行取反
            return cartMapper.selectCartProductCheckedStatusByUserId(userId) == 0;

        }

		/**
	     * 购物车中商品列表
	     */
	    public ServerResponse<CartVo> list (Integer userId){
	        CartVo cartVo = this.getCartVoLimit(userId);
	        return ServerResponse.createBySuccess(cartVo);
	    }
	    //3
	    /**
	     * 更新购物车某个产品数量
	     */
	    public ServerResponse<CartVo> update(Integer userId,Integer productId,Integer count){
	        if(productId == null || count == null){//判断商品ID和商品数量是否为空
	            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
	        }
	        //查询用户购物车中是否有已经要购买的商品
	        Cart cart = cartMapper.selectCartByUserIdProductId(userId,productId);
	        if(cart != null){
	            cart.setQuantity(count);
	        }
	        //执行更新方法
	        cartMapper.updateByPrimaryKey(cart);
	        return this.list(userId);
	    }
	    
	    /**
	     * 删除购物车中商品
	     */
	    public ServerResponse<CartVo> deleteProduct(Integer userId,String productIds){
	    	//购物车中商品之间用","隔开
	        List<String> productList = Splitter.on(",").splitToList(productIds);
	        if(CollectionUtils.isEmpty(productList)){//判断商品是否为空
	            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
	        }
	        //执行删除操作，但是由于是删除购物车中的某一个商品，所以要自己编写SQL语句
	        cartMapper.deleteByUserIdProductIds(userId,productList);
	        return this.list(userId);
	    }
	    //5
	    /**
	     * 购物车选中和选中取消方法
	     */
	    public ServerResponse<CartVo> selectOrUnSelect (Integer userId,Integer productId,Integer checked){
	        cartMapper.checkedOrUncheckedProduct(userId,productId,checked);
	        return this.list(userId);
	    }

	    /**
	     * 查询购物车中商品数量
	     *///6
	    public ServerResponse<Integer> getCartProductCount(Integer userId){
	        if(userId == null){
	            return ServerResponse.createBySuccess(0);
	        }
	        return ServerResponse.createBySuccess(cartMapper.selectCartProductCount(userId));
           }
	    
	    }

 
