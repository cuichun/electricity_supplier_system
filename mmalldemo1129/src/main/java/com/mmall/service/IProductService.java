package com.mmall.service;



import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.vo.ProductDetailVo;


//商品管理
public interface IProductService{
	//商品新增或更新
    ServerResponse saveOrUpdateProduct(Product product);
  //更新商品销售状态
    ServerResponse<String> setSaleStatus(Integer productId,Integer status);
  //根据商品ID查询商品
    ServerResponse<ProductDetailVo> manageProductDetail(Integer productId);
  //分页查询商品列表，根据页数pageNum和每页几条pageSize查询
    ServerResponse<PageInfo> getProductList(int pageNum, int pageSize);
  //根据商品名和商品ID模糊并且分页查询商品
  	ServerResponse<PageInfo> searchProduct(String productName,Integer productId,int pageNum,int pageSize);
  //前台根据商品ID查询商品详情
  	ServerResponse<ProductDetailVo> getProductDetail(Integer productId);
  //前台商品模糊搜索，根据商品品类ID搜索，列表分页，动态排序
    ServerResponse<PageInfo> getProductByKeywordCategory(String keyword,Integer categoryId,int pageNum,int pageSize,String orderBy);

}
