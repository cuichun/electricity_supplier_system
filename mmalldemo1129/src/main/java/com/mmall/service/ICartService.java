package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.vo.CartVo;

public interface ICartService {
	//添加商品到购物车
    ServerResponse<CartVo> add(Integer userId, Integer productId, Integer count);
    //查询购物车中商品列表
    ServerResponse<CartVo> list (Integer userId);
   //更换购物车某个产品数量
	ServerResponse<CartVo> update(Integer id, Integer productId, Integer count);
 //删除购物车中的商品
    ServerResponse<CartVo> deleteProduct(Integer userId,String productIds);
    //购物车选中和选中取消
    ServerResponse<CartVo> selectOrUnSelect (Integer userId,Integer productId,Integer checked);
    //查询购物车中商品数量
    ServerResponse<Integer> getCartProductCount(Integer userId);
  
	
}
