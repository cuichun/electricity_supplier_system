package com.mmall.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.Order;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
    //根据订单号和用户ID查询
    Order selectByUserIdAndOrderNo(@Param("userId")Integer userId,@Param("orderNo")Long orderNo);
    //根据订单号查询订单
    Order selectByOrderNo(Long orderNo);
    //根据用户ID查询订单列表
    List<Order> selectByUserId(Integer userId);
    //后台管理员查询所有订单
    List<Order> selectAllOrder();
}