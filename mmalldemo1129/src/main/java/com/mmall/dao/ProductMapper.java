package com.mmall.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.Category;
import com.mmall.pojo.Product;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);
  //后台查询商品列表
    List<Product> selectList();
  //传入商品名和商品ID进行查询商品
    List<Product> selectByNameAndProductId(@Param("productName")String productName,@Param("productId") Integer productId);
  //编写实现根据商品名称模糊查询和根据商品品类遍历和只查询上架的商品的方法
    List<Product> selectByNameAndCategoryIds(@Param("productName")String productName,@Param("categoryIdList")List<Integer> categoryIdList);
}