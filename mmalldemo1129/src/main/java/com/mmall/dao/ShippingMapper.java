package com.mmall.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.Shipping;

public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);
  //根据用户ID删除收货地址
    int deleteByShippingIdUserId(@Param("userId")Integer userId,@Param("shippingId") Integer shippingId);
  //用户ID更新收货地址，收货地址ID不能修改
    int updateByShipping(Shipping record);
  //根据用户ID和收货地址ID查询地址详情
    Shipping selectByShippingIdUserId(@Param("userId")Integer userId,@Param("shippingId") Integer shippingId);
  //查询收货地址列表
    List<Shipping> selectByUserId(@Param("userId")Integer userId);


}