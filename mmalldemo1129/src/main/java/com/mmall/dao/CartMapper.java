package com.mmall.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.Cart;

public interface CartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);
     //查询用户购物车中是否有已经要购买的商品
    Cart selectCartByUserIdProductId(@Param("userId") Integer userId, @Param("productId")Integer productId);
    //通过用户ID查询购物车
    List<Cart> selectCartByUserId(Integer userId);
    //验证购物车中商品是否被勾选
    int selectCartProductCheckedStatusByUserId(Integer userId);

    //删除购物车中某个商品
    int deleteByUserIdProductIds(@Param("userId") Integer userId,@Param("productIdList")List<String> productIdList);
    //查询购物车中商品数量
    int selectCartProductCount(@Param("userId") Integer userId);
    //选中和取消
    int checkedOrUncheckedProduct(@Param("userId") Integer userId,@Param("productId")Integer productId,@Param("checked") Integer checked);

	List<Cart> selectCheckedCartByUserId(Integer userId);
}