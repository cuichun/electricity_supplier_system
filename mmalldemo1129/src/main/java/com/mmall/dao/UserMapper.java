package com.mmall.dao;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.User;

public interface UserMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(User record);

	int insertSelective(User record);

	User selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);

	// 校验用户名是否存在
	int checkUserName(String username);

	// 用户登录,多个参数时，要用@Param注解声明
	User selectLogin(@Param("username") String username,
			@Param("password") String password);

	// 校邮箱是否存在
	int checkEmail(String eamil);

	// 通过用户名查询出用户的问题
	String selectQuestionByUsername(String username);

	// 校验问题答案
	int checkAnswer(@Param("username") String username,
			@Param("question") String question, @Param("answer") String answer);

	// 根据用户名修改密码
	int updatePasswordByUsername(@Param("username") String username,
			@Param("passwordNew") String passwordNew);

	// 验证旧密码
	int checkPassword(@Param(value = "password") String password,
			@Param("userId") Integer userId);

	// 验证修改过程中的邮箱是否已经被别的用户使用
	int checkEmailByUserId(@Param(value = "email") String email,
			@Param(value = "userId") Integer userId);
	
}