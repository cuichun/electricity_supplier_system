package com.mmall.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mmall.pojo.OrderItem;

public interface OrderItemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderItem record);

    int insertSelective(OrderItem record);

    OrderItem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderItem record);

    int updateByPrimaryKey(OrderItem record);
  //根据订单ID和用户ID查询订单详细
    List<OrderItem> getByOrderNoUserId(@Param("orderNo")Long orderNo, @Param("userId")Integer userId);
    //根据订单号查询订单详细
    List<OrderItem> getByOrderNo(@Param("orderNo")Long orderNo);
    //批量插入订单详情
    void batchInsert(@Param("orderItemList") List<OrderItem> orderItemList);
}