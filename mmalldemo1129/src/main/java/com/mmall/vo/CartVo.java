package com.mmall.vo;

import java.math.BigDecimal;
import java.util.List;

public class CartVo {


	 private List<CartProductVo> cartProductVoList;//封装的购物车列表集合
	    private BigDecimal cartTotalPrice;//购物车总价
	    private Boolean allChecked;//是否已经都勾选
	    private String productMainImage;//商品图片

	    public List<CartProductVo> getCartProductVoList() {
	        return cartProductVoList;
	    }

	    public void setCartProductVoList(List<CartProductVo> cartProductVoList) {
	        this.cartProductVoList = cartProductVoList;
	    }

	    public BigDecimal getCartTotalPrice() {
	        return cartTotalPrice;
	    }

	    public void setCartTotalPrice(BigDecimal cartTotalPrice) {
	        this.cartTotalPrice = cartTotalPrice;
	    }

	    public Boolean getAllChecked() {
	        return allChecked;
	    }

	    public void setAllChecked(Boolean allChecked) {
	        this.allChecked = allChecked;
	    }

		public String getProductMainImage() {
			return productMainImage;
		}

		public void setProductMainImage(String productMainImage) {
			this.productMainImage = productMainImage;
		}

	}



