package com.mmall.vo;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

public class OrderVo {
	private Long orderNo;//订单号

    private BigDecimal payment;//实际付款金额,单位是元

    private Integer paymentType;//支付类型,1-在线支付

    private String paymentTypeDesc;//支付描述
    private Integer postage;//运费,单位是元

    private Integer status;//订单状态:0-已取消-10-未付款，20-已付款，40-已发货


    private String statusDesc;//状态描述

    private String paymentTime;//支付时间

    private String sendTime;//发货时间

    private String endTime;//交易完成时间

    private String closeTime;//关闭时间

    private String createTime;//创建时间

    //订单的明细
    private List<OrderItemVo> orderItemVoList;

    private Integer shippingId;//收货地址ID
    private String receiverName;//收货人姓名
    
    private ShippingVo shippingVo;//收货详情

	public Long getOrderNo() {
		return orderNo;
	}

	public BigDecimal getPayment() {
		return payment;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public String getPaymentTypeDesc() {
		return paymentTypeDesc;
	}

	public Integer getPostage() {
		return postage;
	}

	public Integer getStatus() {
		return status;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public String getPaymentTime() {
		return paymentTime;
	}

	public String getSendTime() {
		return sendTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getCloseTime() {
		return closeTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public List<OrderItemVo> getOrderItemVoList() {
		return orderItemVoList;
	}

	public Integer getShippingId() {
		return shippingId;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public ShippingVo getShippingVo() {
		return shippingVo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public void setPaymentTypeDesc(String paymentTypeDesc) {
		this.paymentTypeDesc = paymentTypeDesc;
	}

	public void setPostage(Integer postage) {
		this.postage = postage;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public void setPaymentTime(String paymentTime) {
		this.paymentTime = paymentTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public void setOrderItemVoList(List<OrderItemVo> orderItemVoList) {
		this.orderItemVoList = orderItemVoList;
	}

	public void setShippingId(Integer shippingId) {
		this.shippingId = shippingId;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public void setShippingVo(ShippingVo shippingVo) {
		this.shippingVo = shippingVo;
	}
}
