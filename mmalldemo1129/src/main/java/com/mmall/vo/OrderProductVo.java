package com.mmall.vo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
public class OrderProductVo {
	   private List<OrderItemVo> orderItemVoList;//订详细单集合
	    private BigDecimal productTotalPrice;//商品总价格

	    
	    public List<OrderItemVo> getOrderItemVoList() {
	        return orderItemVoList;
	    }

	    public void setOrderItemVoList(List<OrderItemVo> orderItemVoList) {
	        this.orderItemVoList = orderItemVoList;
	    }

	    public BigDecimal getProductTotalPrice() {
	        return productTotalPrice;
	    }

	    public void setProductTotalPrice(BigDecimal productTotalPrice) {
	        this.productTotalPrice = productTotalPrice;
	    }
}
